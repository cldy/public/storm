import { addDecorator } from '@storybook/react'
// noinspection ES6CheckImport
import { withThemes } from '@react-theming/storybook-addon'
import { theme } from '../src'
import { ThemeProvider } from 'emotion-theming'

addDecorator(withThemes(ThemeProvider, [theme]))
export default {
    title: 'Component',
}