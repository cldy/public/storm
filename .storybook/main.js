const path = require('path')

module.exports = {
    stories: ['../src/**/*.stories.[tj]sx'],
    addons: [
        // '@storybook/preset-typescript',
        '@react-theming/storybook-addon',
        '@storybook/addon-a11y',
        '@storybook/addon-actions',
        '@storybook/addon-backgrounds',
        '@storybook/addon-docs',
        '@storybook/addon-links',
        '@storybook/addon-notes',
        '@storybook/addon-storysource',
        '@storybook/addon-viewport',
    ],
    webpackFinal: async config => {
        config.module.rules.push({
            test: /\.(ts|tsx)$/,
            use: [
                {
                    loader: require.resolve('ts-loader'),
                    options: {
                        configFile: path.resolve(__dirname, "../tsconfig.json"),
                    },
                },
                {
                    loader: require.resolve('react-docgen-typescript-loader'),
                    options: {
                        tsconfigPath: path.resolve(__dirname, "../tsconfig.json"),
                    },
                },
            ],
        })
        config.resolve.extensions.push('.ts', '.tsx')
        return config
    },
}