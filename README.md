# Storm

**Storm is a design system and component library for use with [Next.js](https://nextjs.org). It includes theme primitives, a colour system, base components (Box, Flex), and some useful utilities.**

Use with plain React or other frameworks is possible, but not supported.

Built using [Emotion](https://emotion.sh), [Styled System](https://styled-system.com/getting-started) and TypeScript.

**A Storybook of the included components with a full list of exposed props is available at [storm.cloudey.dev](https://storm.cloudey.dev)**.

*This package is under active development and should not be used in production (yet). Any part of the package can be changed or removed at any time, with no guarantee of backwards compatibility.*

## Usage with Next.js

This guide assumes that your project is using Next.js, Typescript, has the peer dependencies required by this package installed, and that you have a basic knowledge of React and Next.js.

### Basic setup

This package includes a theme specification, and some global styles to normalise behaviour across browsers (normalize.css).  
To set up the theme and include the global styles for your application, create or modify your `pages/_app.tsx` file to look something like this:
```typescript jsx
import { AppProps } from 'next/app'
import * as React from 'react'
import { ThemeProvider } from 'emotion-theming'
import { theme, global } from '@cloudey/storm'
import { Global } from '@emotion/core'

export default function App ({Component, pageProps}: AppProps) {
    return (
        <ThemeProvider theme={theme}>
            <Global styles={global}/>
            <Component {...pageProps}/>
        </ThemeProvider>
    )
}
```

### Using the Box component

The `Box` components is the basic building block for other, more complex components, inspired by `superbox` and `reflexbox`.  
You can use the `Box` component to quickl y create reusable or one-time styled components with [Emotion](https://emotion.sh) and/or [Styled System](https://styled-system.com/getting-started) props.  
By default, `Box` is a `div` element. You can specify the `as` prop to use any other element instead.

Example:
```typescript jsx
import React from 'react'
import { Box } from '@cloudey/storm'

export default function Home () {
    return (
        <Box bg={'red'} p={4}>
            <Box bg={'blue'} p={4} m={2} fontWeight={'bold'}>
                This div has a blue background, a scale 4 (16px) padding, a scale 2 (8px) margin, and bold text.
            </Box>
            <Box as={'p'} fontSize={'xl'}>
                This is an HTML 'p' element with extra large text.
            </Box>
        </Box>
    )
}
```

### Using the Flex component

The `Flex` component is a simple `Box` component with a `display: flex` attribute. It exposes the same props and is used exactly like a `Box`, including the `as` prop.

### Using the Grid component

The `Grid` component is a simple `Box` component with a `display: grid` attribute. It exposes the same props and is used exactly like a `Box`, including the `as` prop. You can specify the grid layout with the usual props (gridTemplateRows, etc.).  
Variants for predefined grid layouts and shorthand props will be added in the future.

### Extending components

To extend the `Box` component to create your own components, use the `styled` function provided by `@emotion/styled`. [Read more here](https://emotion.sh/docs/styled).

### Using a custom theme

You can easily override specific theme keys and even replace the entire included theme. Simply provide your own theme to the ThemeProvider in `pages/_app.ts`. All components will automatically use the provided theme.  
**Make sure your custom theme follows the [Styled System theme specification](https://styled-system.com/theme-specification).**

## Examples

An example boilerplate project using Next.js and this library is available here: [next-base](https://gitlab.com/cldy/public/next-base).

## Legal stuff

Licensed under MIT, provided in the `LICENSE` file.  
Created by [Cloudey](https://cloudey.net).  
Cloudey® is a registered trademark of Cloudey IT Ltd.