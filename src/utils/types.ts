import {
    BackgroundProps,
    BorderProps,
    ColorProps,
    FlexboxProps,
    GridProps,
    LayoutProps,
    PositionProps,
    ShadowProps,
    SpaceProps,
    Theme,
    TypographyProps,
} from 'styled-system'
import React, { ClassAttributes, HTMLAttributes } from 'react'

export const styleProps = [
    'space',
    'margin',
    'marginTop',
    'marginBottom',
    'marginLeft',
    'marginRight',
    'padding',
    'paddingTop',
    'paddingBottom',
    'paddingLeft',
    'paddingRight',
    'width',
    'fontSize',
    'textColor',
    'backgroundColor',
    'color',
    'fontFamily',
    'textAlign',
    'lineHeight',
    'fontWeight',
    'fontStyle',
    'letterSpacing',
    'display',
    'maxWidth',
    'minWidth',
    'height',
    'maxHeight',
    'minHeight',
    'size',
    'verticalAlign',
    'alignItems',
    'alignContent',
    'justifyItems',
    'justifyContent',
    'flexWrap',
    'flexBasis',
    'flexDirection',
    'flex',
    'justifySelf',
    'alignSelf',
    'order',
    'gridGap',
    'gridColumnGap',
    'gridRowGap',
    'gridColumn',
    'gridRow',
    'gridAutoFlow',
    'gridAutoColumns',
    'gridAutoRows',
    'gridTemplateColumns',
    'gridTemplateRows',
    'gridTemplateAreas',
    'gridArea',
    'border',
    'borderTop',
    'borderRight',
    'borderBottom',
    'borderLeft',
    'borders',
    'borderColor',
    'borderRadius',
    'borderWidth',
    'borderStyle',
    'boxShadow',
    'opacity',
    'overflow',
    'background',
    'backgroundImage',
    'backgroundPosition',
    'backgroundRepeat',
    'backgroundSize',
    'position',
    'zIndex',
    'top',
    'right',
    'bottom',
    'left',
    'textStyle',
    'colorStyle',
    'buttonStyle',
    'm',
    'p',
    'mt',
    'mr',
    'mb',
    'ml',
    'mx',
    'my',
    'pt',
    'pr',
    'pb',
    'pl',
    'px',
    'py',
    'bg',
]

export type StyleProps =
    SpaceProps &
    ColorProps &
    TypographyProps &
    LayoutProps &
    FlexboxProps &
    GridProps &
    BackgroundProps &
    BorderProps &
    PositionProps &
    ShadowProps

export type ComponentProps = Partial<
    ClassAttributes<any> &
    HTMLAttributes<any>
>

export type StyledComponentProps = Partial<
    StyleProps &
    ComponentProps &
    {
        theme?: Theme,
        as: keyof JSX.IntrinsicElements | React.ComponentType<any>,
    }
>

