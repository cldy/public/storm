import emotionStyled, { CreateStyled } from '@emotion/styled'
import { Theme } from '../'

export const styled: CreateStyled<Theme> = emotionStyled