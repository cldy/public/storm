import { darken, lighten } from 'polished'

export const variants: (string) => IVariants = (color: string) => {
    const colors = {500: color}
    for (let i = 10; i < 500; i = i + 10) {
        colors[500 - i] = lighten(i / 1000, color)
        colors[500 + i] = darken(i / 1000, color)
    }
    return colors
}

export interface IVariants {
    [x: number]: string,
}
