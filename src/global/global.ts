import { fonts, fontSizes } from '../primitives'
import { normalize } from 'polished'

const global = {
    ...normalize()[0],
}

global['html']['fontSize'] = fontSizes.default
global['html']['fontFamily'] = fonts.default
global['body']['minHeight'] = '100vh'

export { global }
