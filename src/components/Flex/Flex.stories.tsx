import React from 'react'
import { Flex } from '.'

export default {
    title: 'Flex',
    displayName: 'Flex',
    component: Flex,
}

export const Default = () => {
    return (
        <Flex>This is a Flex</Flex>
    )
}

export const withChildren = () => {
    return (
        <Flex p={4} bg={'lightGray'}>
            <Flex p={2} mx={1} bg={'blue'}>One</Flex>
            <Flex p={2} mx={1} bg={'blue'}>Two</Flex>
            <Flex p={2} mx={1} bg={'blue'}>Three</Flex>
        </Flex>
    )
}
