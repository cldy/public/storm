import { Box, BoxProps } from '../'
import { withTheme } from 'emotion-theming'
import styled from '@emotion/styled'

export const Flex = withTheme(styled(Box)(
    {
        display: 'flex',
    },
))

export type FlexProps = BoxProps
