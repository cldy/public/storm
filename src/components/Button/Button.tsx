import React from 'react'
import { Box, BoxProps } from '../Box'
import { withTheme } from 'emotion-theming'
import { styled } from '../..'

export interface ButtonProps extends BoxProps {

}

const SButton = withTheme(styled(Box)(({theme}) => ({
    paddingTop: theme.space[3],
    paddingBottom: theme.space[3],
    paddingLeft: theme.space[6],
    paddingRight: theme.space[6],
    border: theme.borders.none,
    borderRadius: theme.radii.xs,
    backgroundColor: theme.colors.blues[500],
    color: theme.colors.white,
    textTransform: 'uppercase',
    fontFamily: theme.fonts.default,
    fontWeight: theme.fontWeights.extrabold,
    transition: '200ms all',
    ':hover, :focus': {
        backgroundColor: theme.colors.blues[600],
        cursor: 'pointer',
    },
    ':active': {
        backgroundColor: theme.colors.blues[700],
    },
})))

export const Button = ({children, ...props}: ButtonProps) => {
    return (
        <SButton as={'button'}>
            { children }
        </SButton>
    )
}
