import { Button } from './Button'
import React from 'react'

export default {
    title: 'Button',
    displayName: 'Button',
    component: Button,
}

export const Default = () => {
    return (
        <Button>Button</Button>
    )
}
