import { Box, BoxProps } from '../'
import { withTheme } from 'emotion-theming'
import styled from '@emotion/styled'

export const Grid = withTheme(styled(Box)(
    {
        display: 'grid',
    },
))

export type GridProps = BoxProps
