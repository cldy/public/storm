import React from 'react'
import { Grid } from '.'
import { Box } from '../Box'

export default {
    title: 'Grid',
    displayName: 'Grid',
    component: Grid,
}

export const Default = () => {
    return (
        <Grid>This is a Grid</Grid>
    )
}

export const with3Columns = () => {
    return (
        <Grid p={4} bg={'lightGray'} gridTemplateColumns={'1fr 1fr 1fr'}>
            <Box p={2} m={1} bg={'blue'}>One</Box>
            <Box p={2} m={1} bg={'blue'}>Two</Box>
            <Box p={2} m={1} bg={'blue'}>Three</Box>
            <Box p={2} m={1} bg={'blue'}>Four</Box>
        </Grid>
    )
}
