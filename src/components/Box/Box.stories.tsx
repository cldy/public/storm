import React from 'react'
import { Box } from '.'
import { styled } from '../..'

export default {
    title: 'Box',
    displayName: 'Box',
    component: Box,
}

export const Default = () => {
    return (
        <Box>This is a Box</Box>
    )
}

export const WithPadding = () => {
    return (
        <Box p={4} bg={'blue'}>Padding 4</Box>
    )
}

export const AsAnotherElement = () => {
    return (
        <Box as={'h1'} p={4} bg={'blues.400'}>This is an h1 element</Box>
    )
}

export const WithStyled = () => {
    const StyledBox = styled(Box)(({theme}) => ({
        padding: theme.space[4],
        backgroundColor: theme.colors.blue,
    }))

    return (
        <StyledBox>This is a Box</StyledBox>
    )
}