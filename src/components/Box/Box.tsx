import { styled } from '../..'
import { background, border, color, flexbox, grid, layout, position, shadow, space, typography } from 'styled-system'
import { withTheme } from 'emotion-theming'
import React from 'react'
import { StyledComponentProps, styleProps } from '../../utils/types'

export type BoxProps = StyledComponentProps

export const Box: React.FunctionComponent<BoxProps> = withTheme(styled('div', {
    shouldForwardProp: (propName: string): boolean => {
        return propName !== 'as' && propName !== 'theme' && !styleProps.includes(propName)
    },
})(
    {
        boxSizing: 'border-box',
        minWidth: 0,
    },
    space,
    color,
    typography,
    layout,
    flexbox,
    grid,
    background,
    border,
    position,
    shadow,
))
