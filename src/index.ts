import * as primitives from './primitives'
import {Theme as StyledSystemTheme} from 'styled-system'

export const theme = {
    name: 'Storm',
    ...primitives,
}

export { global } from './global/global'
export { styled } from './utils/styled'
export * from './components'
export { primitives }
export type Theme = typeof theme & StyledSystemTheme
