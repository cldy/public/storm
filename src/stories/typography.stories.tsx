import { Box, theme } from '../index'
import React from 'react'

export default {
    title: 'Typography',
    displayName: 'Typography',
    parameters: {
        notes: 'Note that font weights and font families are only displayed correctly if the corresponding fonts are installed on your machine. ' +
            'This example does not fetch fonts automatically, as neither does this library.',
    },
}

export const FontSizes = () => {
    const Examples = () => {
        return Object.keys(theme.fontSizes).map(key => (
            <Box key={key} fontSize={key} my={4}>
                {key}: Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            </Box>
        ))
    }

    return (
        <Box>
            {Examples()}
        </Box>
    )
}

export const FontWeights = () => {
    const Examples = () => {
        return Object.keys(theme.fontWeights).map(key => (
            <Box key={key} fontWeight={key} my={4}>
                {key}: Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            </Box>
        ))
    }

    return (
        <Box>
            {Examples()}
        </Box>
    )
}

export const Fonts = () => {
    const Examples = () => {
        return Object.keys(theme.fonts).map(key => (
            <Box key={key} fontFamily={key} my={4}>
                {key}: Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            </Box>
        ))
    }

    return (
        <Box>
            {Examples()}
        </Box>
    )
}

export const LetterSpacings = () => {
    const Examples = () => {
        return Object.keys(theme.letterSpacings).map(key => (
            <Box key={key} letterSpacing={key} my={4}>
                {key}: Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            </Box>
        ))
    }

    return (
        <Box>
            {Examples()}
        </Box>
    )
}

export const LineHeights = () => {
    const Examples = () => {
        return Object.keys(theme.lineHeights).map(key => (
            <Box key={key} lineHeight={key} my={4} width={8}>
                {key}: Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                     Accusantium ad adipisci aliquid animi autem beatae cupiditate delectus distinctio dolorem ducimus est facere,
                     facilis iusto laboriosam maiores, minima perspiciatis quibusdam, quidem.
            </Box>
        ))
    }

    return (
        <Box>
            {Examples()}
        </Box>
    )
}