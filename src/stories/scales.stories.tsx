import { Box, Flex, theme } from '../index'
import React from 'react'

export default {
    title: 'Scales',
    displayName: 'Scales',
}

export const Sizes = () => {
    const Examples = () => {
        return Object.keys(theme.sizes).map(key => (
            <Flex key={key} width={key} height={key} bg={'lightGray'} m={8} alignItems={'center'} justifyContent={'center'}>
                {key}
            </Flex>
        ))
    }

    return (
        <Box>
            {Examples()}
        </Box>
    )
}

export const Space = () => {
    const Examples = () => {
        return Object.keys(theme.space).map(key => (
            <Flex key={key} my={2} bg={'blue'} width={'min-content'}>
                <Flex width={4} height={4} bg={'lightGray'} mr={key} alignItems={'center'} justifyContent={'center'}>
                    {key}
                </Flex>
                <Box width={0} height={4}/>
            </Flex>

        ))
    }

    return (
        <Box>
            {Examples()}
        </Box>
    )
}