import { Box, Flex, theme } from '../index'
import React from 'react'

export default {
    title: 'Shadows',
    displayName: 'Shadows',
}

export const Shadows = () => {
    const Examples = () => {
        return Object.keys(theme.shadows).map(key => (
            <Box boxShadow={key} background={'lightGray'} width={6} p={6} m={6} fontWeight={'bold'} textAlign={'center'} key={key}>
                {key}
            </Box>
        ))
    }

    return (
        <Flex flexWrap={'wrap'}>
            {Examples()}
        </Flex>
    )
}