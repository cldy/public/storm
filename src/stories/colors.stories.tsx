import React from 'react'
import { useTheme } from 'emotion-theming'
import { theme, Flex, Box } from '..'
import {readableColor, parseToRgb} from 'polished'

export default {
    title: 'Colors',
    displayName: 'Colors',
    parameters: {
        notes: 'The color scale actually uses increments of 10. Colors are displayed here in increments of 50 for the sake of providing a better overview.'
    },
}

const ColorBoxes = (color: string) => {
    const boxes = []
    for (let i = 50; i < 1000; i = i + 50) {
        const themeColor = theme.colors[`${color}s`][i]
        const rgb = parseToRgb(theme.colors[`${color}s`][i]) 
        boxes.push(
            <Box bg={`${color}s.${i}`} py={2} key={i} fontFamily={'monospace'} color={readableColor(themeColor, 'black', 'white')}>
                <Flex justifyContent={'space-between'} maxWidth={800} mx={'auto'} flexWrap={'wrap'} position={'relative'}>
                    <Box width={'33.3%'}>
                        {themeColor}
                    </Box>
                    <Box fontWeight={'bold'} width={'33.3%'}>
                        {i}
                    </Box>
                    <Box width={'33.3%'}>
                        rgb({rgb.red}, {rgb.green}, {rgb.blue})
                    </Box>
                </Flex>
            </Box>
        )
    }
    return boxes
}

export const gray = () => (
    <Box>
        {ColorBoxes('gray')}
    </Box>
)

export const lightGray = () => (
    <Box>
        {ColorBoxes('lightGray')}
    </Box>
)

export const darkGray = () => (
    <Box>
        {ColorBoxes('darkGray')}
    </Box>
)

export const blueGray = () => (
    <Box>
        {ColorBoxes('blueGray')}
    </Box>
)

export const red = () => (
    <Box>
        {ColorBoxes('red')}
    </Box>
)

export const orange = () => (
    <Box>
        {ColorBoxes('orange')}
    </Box>
)

export const yellow = () => (
    <Box>
        {ColorBoxes('yellow')}
    </Box>
)

export const green = () => (
    <Box>
        {ColorBoxes('green')}
    </Box>
)

export const teal = () => (
    <Box>
        {ColorBoxes('teal')}
    </Box>
)

export const blue = () => (
    <Box>
        {ColorBoxes('blue')}
    </Box>
)

export const indigo = () => (
    <Box>
        {ColorBoxes('indigo')}
    </Box>
)

export const purple = () => (
    <Box>
        {ColorBoxes('purple')}
    </Box>
)

export const pink = () => (
    <Box>
        {ColorBoxes('pink')}
    </Box>
)