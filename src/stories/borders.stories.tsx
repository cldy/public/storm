import { Box } from '../components/Box'
import React from 'react'
import { Flex } from '../components/Flex'
import { theme } from '../index'

export default {
    title: 'Borders',
    displayName: 'Borders',
}

export const Borders = () => {
    const Examples = () => {
        return Object.keys(theme.borders).map(key => (
            <Box border={key} background={'lightGray'} width={6} p={6} m={3} fontWeight={'bold'} textAlign={'center'} key={key}>
                {key}
            </Box>
        ))
    }

    return (
        <Flex flexWrap={'wrap'}>
            {Examples()}
        </Flex>
    )
}

export const BorderStyles = () => {
    const Examples = () => {
        return Object.keys(theme.borderStyles).map(key => (
            <Box border={2} borderStyle={key} background={'lightGray'} width={6} p={6} m={3} fontWeight={'bold'} textAlign={'center'} key={key}>
                {key}
            </Box>
        ))
    }

    return (
        <Flex flexWrap={'wrap'}>
            {Examples()}
        </Flex>
    )
}

export const BorderWidths = () => {
    const Examples = () => {
        return Object.keys(theme.borderWidths).map(key => (
            <Box border={2} borderWidth={key} background={'lightGray'} width={6} p={6} m={3} fontWeight={'bold'} textAlign={'center'} key={key}>
                {key}
            </Box>
        ))
    }

    return (
        <Flex flexWrap={'wrap'}>
            {Examples()}
        </Flex>
    )
}

export const Radii = () => {
    const Examples = () => {
        return Object.keys(theme.radii).map(key => (
            <Box border={2} borderRadius={key} background={'lightGray'} width={6} p={6} m={3} fontWeight={'bold'} textAlign={'center'} key={key}>
                {key}
            </Box>
        ))
    }

    return (
        <Flex flexWrap={'wrap'}>
            {Examples()}
        </Flex>
    )
}
