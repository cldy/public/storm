export const fonts = {
    display: '"Work Sans", "Open Sans", sans-serif',
    default: '"Nunito", "Nunito Sans", "Open Sans", sans-serif',
    monospace: '"JetBrains Mono", "Fira Code", "Fira Mono", monospace',
    workSans: '"Work Sans"',
    nunito: '"Nunito"',
    nunitoSans: '"Nunito Sans"',
    openSans: '"Open Sans"',
    jetBrainsMono: '"JetBrains Mono"',
}
