export const lineHeights = Object.assign({},
    [1, 1.25, 1.375, 1.5, 1.625, 2],
    {
        none: 1,
        xs: 1.25,
        sm: 1.375,
        md: 1.5,
        lg: 1.625,
        xl: 2,
    },
)
