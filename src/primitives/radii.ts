export const radii = Object.assign({},
    [0, '0.2rem', '0.4rem', '0.6rem', '0.8rem', '1rem'],
    {
        none: 0,
        xs: '0.2rem',
        sm: '0.4rem',
        md: '0.6rem',
        lg: '0.8rem',
        xl: '1rem',
        full: '9999px',
    },
)
