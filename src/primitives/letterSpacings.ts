export const letterSpacings = Object.assign({},
    [0, '-0.05em', '-0.025em', '0.025em', '0.05em', '0.1em'],
    {
        default: 0,
        tighter: '-0.05em',
        tight: '-0.025em',
        wide: '0.025em',
        wider: '0.05em',
        widest: '0.1em',
    },
)
