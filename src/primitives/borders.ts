export const borders = {
    none: 'none',
    0: 'none',
    1: '1px solid',
    2: '2px solid',
    3: '3px solid',
    4: '4px solid',
    5: '5px solid',
    6: '6px solid',
    default: '1px solid',
}
