export const fontSizes = Object.assign({},
    [8, 12, 14, 16, 20, 24, 32, 48],
    {
        xs: 10,
        sm: 12,
        md: 14,
        lg: 16,
        xl: 20,
        xl2: 24,
        xl3: 32,
        xl4: 48,
        default: 14,
    },
)
