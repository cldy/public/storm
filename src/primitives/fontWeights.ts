export const fontWeights = Object.assign({},
    [100, 200, 300, 400, 500, 600, 700, 800, 900],
    {
        thin: 100,
        extralight: 200,
        light: 300,
        regular: 400,
        medium: 500,
        semibold: 600,
        bold: 700,
        extrabold: 800,
        black: 900,
    },
)
