export const sizes = Object.assign({},
    [0, 4, 8, 16, 32, 64, 128, 256, 512],
    {
        px: 1,
    },
)
