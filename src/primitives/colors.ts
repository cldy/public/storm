import { variants } from '../utils/color'

export const colors = {
    black: '#000000',
    white: '#ffffff',
    gray: '#808090',
    lightGray: '#c0c0c8',
    darkGray: '#404048',
    blueGray: '#404050',
    red: '#f91a06',
    orange: '#ffaa00',
    yellow: '#ffe600',
    green: '#13ec66',
    teal: '#17e8b4',
    blue: '#1228ed',
    indigo: '#2f12ed',
    purple: '#5a01fe',
    pink: '#ff005b',
    brand: {
        500: '#1228ed',
        600: '#0e20be',
        700: '#0b188e',
    },
    get primary () {
        return this.brand[500]
    },
    get grays () {
        delete this.grays
        return this.grays = variants(this.gray)
    },
    get lightGrays () {
        delete this.lightGrays
        return this.lightGrays = variants(this.lightGray)
    },
    get darkGrays () {
        delete this.darkGrays
        return this.darkGrays = variants(this.darkGray)
    },
    get blueGrays () {
        delete this.blueGrays
        return this.blueGrays = variants(this.blueGray)
    },
    get reds () {
        delete this.reds
        return this.reds = variants(this.red)
    },
    get oranges () {
        delete this.oranges
        return this.oranges = variants(this.orange)
    },
    get yellows () {
        delete this.yellows
        return this.yellows = variants(this.yellow)
    },
    get greens () {
        delete this.greens
        return this.greens = variants(this.green)
    },
    get teals () {
        delete this.teals
        return this.teals = variants(this.teal)
    },
    get blues () {
        delete this.blues
        return this.blues = variants(this.blue)
    },
    get indigos () {
        delete this.indigos
        return this.indigos = variants(this.indigo)
    },
    get purples () {
        delete this.purples
        return this.purples = variants(this.purple)
    },
    get pinks () {
        delete this.pinks
        return this.pinks = variants(this.pink)
    },
}
