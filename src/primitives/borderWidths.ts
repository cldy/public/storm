export const borderWidths = Object.assign({},
    [0, 1, 2, 3, 4, 5, 6],
    {
        default: 1,
    },
)
